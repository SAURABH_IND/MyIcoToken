const HDWalletProvider = require("truffle-hdwallet-provider-privkey");
const privateKey= "2d542b7957e308ba8693c2731337a27739f91f90d93af83982f7d6d5dacf97d6";
const publicKey = "0xF48137fCF70dF9e3eE119ADE445279CdCBE0735b";

module.exports = {
  	networks: {
    	rinkeby: {
      		provider: function() {
        		return new HDWalletProvider(privateKey,"https://rinkeby.infura.io/metamask")
      			},
      		network_id: 4
   		},
    	development: {
      		host: "127.0.0.1",
      		port: 7545,
      		network_id: "*" // Match any network id
    	}
  	}
};

