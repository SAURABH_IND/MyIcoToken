pragma solidity ^0.4.21;

import "./ERC20.sol";
import "./SafeMath.sol";
import "./Ownable.sol";

contract MyIcoToken is ERC20, Ownable {
    using SafeMath for uint;

    string public name = "MY ICO TOKEN";
    string public symbol = "MY_ICO_TOKEN";
    uint8 constant public decimals = 0;
    uint private _totalSupply = 1000000000000000;
    

    mapping(address => uint) balances;
    mapping(address => mapping(address => uint)) allowed;
    
    
    mapping(address => bool) public WhiteAddress;
    
    function AddWhiteAddress(address _address) public onlyOwner  
    {
        WhiteAddress[_address] = true;
    }
    
    function RemoveAddress(address _address) public onlyOwner 
    {   
        WhiteAddress[_address] = false;
    }
    

    
    function MyIcoToken() public {
        balances[owner] = _totalSupply;
    }

    function totalSupply() public constant returns (uint) {
        return _totalSupply - balances[address(0)];
    }

    function balanceOf(address tokenOwner) public constant returns (uint balance) {
        return balances[tokenOwner];
    }

    function issue(address to, uint tokens) public onlyOwner {
        if (to == address(0) || to == owner) {
            revert();
        }

        balances[to] = balances[to].add(tokens);
        _totalSupply = _totalSupply.add(tokens);
    }

    function transfer(address to, uint tokens) public  returns (bool success) {
        require(WhiteAddress[to]==true);
        balances[msg.sender] = balances[msg.sender].sub(tokens);
        if (to == owner) 
        {
            _totalSupply = _totalSupply.sub(tokens);
        } 
        else 
        {
            balances[to] = balances[to].add(tokens);
            emit Transfer(msg.sender, to, tokens);
        }
        return true;
    }
    
    function burn(uint _value) public returns (bool success) {
        require(_value > 0);
        require(_value <= balances[msg.sender]);
        address burner = msg.sender;
        balances[burner] = balances[burner].sub(_value);
        _totalSupply = _totalSupply.sub(_value);
        emit Burn(burner,_value);
        return true;
    }


    function approve(address spender, uint tokens) public  returns (bool success) {
        require(WhiteAddress[spender]==true);
        allowed[msg.sender][spender] = tokens;
        emit Approval(msg.sender, spender, tokens);
        return true;
    }
    
    function transferFrom(address from, address to, uint tokens) public  returns (bool success) {
        require(WhiteAddress[to]==true);
        balances[from] = balances[from].sub(tokens);
        allowed[from][msg.sender] = allowed[from][msg.sender].sub(tokens);
        balances[to] = balances[to].add(tokens);
        emit Transfer(from, to, tokens);
        return true;
    }

    function allowance(address tokenOwner, address spender) public constant returns (uint remaining) {
        return allowed[tokenOwner][spender];
    }
    
    
    

    // ------------------------------------------------------------------------
    // Don't accept ETH
    // ------------------------------------------------------------------------
    function () public payable {
        revert();
    }
}

