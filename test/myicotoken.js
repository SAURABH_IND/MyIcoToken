let MyIcoToken = artifacts.require("./MyIcoToken.sol");

contract('MyIcoToken', (accounts) => {
  var contract;

  beforeEach(async () => 
  {
    contract = await MyIcoToken.new();

    let totalSupply = await contract.totalSupply.call();
    assert.equal(totalSupply.toNumber(),1000000000000000,'total supply is')

    let balance0 = await contract.balanceOf.call(accounts[0]);
    assert.equal(balance0.toNumber(), 1000000000000000, 'owner balance is wrong');
  });

  it("should not accept ethers (nonpayable)",async () => {
    try{
      result = await contract.send(web3.toWei(1,"wei"));
      assert(false,'should not accept ether');
    }
    catch(error){
      assert(true);
    }
  });

  it("should not allow transfer of tokens from owner(issue)", async () => {
    try {
      await contract.AddWhiteAddress(accounts[0]);
      await contract.transfer(accounts[0],1000000000000);
      await contract.transfer(accounts[1],10000000000000000000);
      assert(false,"should not allowed to transfer more than assign tokens");
    } 
    catch (error) {
      assert(true);
    }
  });

  it("should transfer ownership (ownable)", async () => {
    try {
      await contract.transferOwnership(accounts[1]);
      await contract.issue(accounts[2], 100000);
      assert(false, "should not have allowed issue from account0 since owner has changed");
    } catch (e) {
      assert(true);
    }
  });

  it("should be destroy the contract",async () => {
    try{
      await contract.destroy();
      await contract.issue(accounts[3],1000000);
      assert(false,"owner is not destroy");
    }
    catch(error){
      assert(true);
    }
  });

  it("should not transfer after added to RemoveList", async () => {
    try{
      await contract.RemoveList(accounts[1]);
      await contract.transfer(accounts[1],10000);
      assert(false,'accounts is not added to transfer funciton');
    }
    catch(error){
      assert(true);
    }
  });

  it("Transfer after add the addtess in AddWhiteList", async () => {
    await contract.AddWhiteAddress(accounts[1]);
    await contract.transfer(accounts[1],10000);

    let balance1 = await contract.balanceOf.call(accounts[1]);
    assert.equal(balance1.toNumber(),10000,'Address is successfully address')

    let address = await contract.WhiteAddress.call(accounts[1]);
    assert.equal(address.toString(),'true','Address is successfully addedd to AddWhiteList');
  })

  it("Add and Remove and again add the address from whitelist to remove list", async () =>
  {
    await contract.AddWhiteAddress(accounts[1]);
    let address = await contract.WhiteAddress.call(accounts[1]);
    assert.equal(address.toString(),'true','Address is addred to WhiteList');

    await contract.RemoveAddress(accounts[1]);
    let address1 = await contract.WhiteAddress.call(accounts[1]);
    assert.equal(address1.toString(),'false','Address is successfully remove from WhiteList');

    await contract.AddWhiteAddress(accounts[1]);
    let address2 = await contract.WhiteAddress.call(accounts[1]);
    assert.equal(address2.toString(),'true','Address is again add to White List');
  });



  it("Transfer tokens from owner or who have tokens",async () => {
    await contract.AddWhiteAddress(accounts[1]);
    await contract.transfer(accounts[1],10000000000);

    let balance1 = await contract.balanceOf.call(accounts[1]);
    assert.equal(balance1.toNumber(),10000000000,'balance is not transfer');

    let balance2 = await contract.balanceOf.call(accounts[0]);
    assert.equal(balance2.toNumber(),999990000000000,'balance is not transfer from owner accounts');

  });

  it("should record the approve event (Approval)",async () => {
    await contract.AddWhiteAddress(accounts[4]);
    await contract.issue(accounts[4],6000);
    let balance = await contract.totalSupply.call();
    assert.equal(balance.toNumber(),1000000000006000,'total supply is wrong');
    let balance1 = await contract.balanceOf.call(accounts[4]);
    assert.equal(balance1.toNumber(),6000,'total balance is wrong');
    let balance2 = await contract.balanceOf.call(accounts[0]);
    assert.equal(balance2.toNumber(),1000000000000000,'total supply is wrong');

    let approve = await contract.approve(accounts[4],6000,{from:accounts[0]});
    let events = approve.logs[0];
    assert.equal(events.event,'Approval');
    assert.equal(events.args.tokenOwner,accounts[0]);
    assert.equal(events.args.spender,accounts[4]);
    assert.equal(events.args.tokens,6000);

  });

  it("should record the transfer event (Transfer)",async () => {
    await contract.issue(accounts[2],56000);
    let totalSupply = await contract.totalSupply.call();
    assert.equal(totalSupply.toNumber(),1000000000056000,'total supply is wrong');
    let balance1 = await contract.balanceOf.call(accounts[2]);
    assert.equal(balance1.toNumber(),56000,'total balance is wrong');
    let balance2 = await contract.balanceOf.call(accounts[0]);
    
    await contract.AddWhiteAddress(accounts[2]);
    let transfer = await contract.transfer(accounts[2],56000,{from:accounts[0]});
    let events = transfer.logs[0];
    assert.equal(events.event,'Transfer');
    assert.equal(events.args.from,accounts[0]);
    assert.equal(events.args.to,accounts[2]);
    assert.equal(events.args.tokens,56000);
  });

  it("should give accounts[1] authority to spend account[0] token(approve, allowance, transferfrom)",async() => {
    await contract.issue(accounts[1],1000);
    let totalSupply = await contract.totalSupply.call();
    assert.equal(totalSupply.toNumber(),1000000000001000,'balance is not transfer');
    let balance1 = await contract.balanceOf.call(accounts[1]);
    assert.equal(balance1.toNumber(),1000,'balance is not get');
    let balance2 = await contract.balanceOf.call(accounts[0]);
    assert.equal(balance2.toNumber(),1000000000000000,'balance is not transfer by owner');

    await contract.AddWhiteAddress(accounts[2]);
    await contract.approve(accounts[2],3000,{from:accounts[1]});
    let allowance = await contract.allowance.call(accounts[1],accounts[2]);
    assert.equal(allowance.toNumber(),3000,'allowance is wrong');
  });

  it("Burn total left tokens", async () => {
      let balance = await contract.balanceOf.call(accounts[0]);
      assert.equal(balance.toNumber(),1000000000000000,'total supply is not correct');

      // Burn or transfer all the tokens to the unknown accounts, through owner only.
      await contract.burn(1000000000000000);

      // Totlal tokens is zero after burn funciton.
      let balance1 = await contract.balanceOf.call(accounts[0]);
      assert.equal(balance1.toNumber(),0,'tokens is not burn');
    });

   it("should give accounts[1] authority to spend account[0]'s token (approve, allowance, transferFrom )", async () => {
    await contract.issue(accounts[1], 5000);
    let totalSupply = await contract.totalSupply.call();
    assert.equal(totalSupply.toNumber(), 1000000000005000, 'total supply is wrong');
    let balance1 = await contract.balanceOf.call(accounts[1]);
    assert.equal(balance1.toNumber(), 5000, 'balance is wrong'); 
    let balance0 = await contract.balanceOf.call(accounts[0]);
    assert.equal(balance0.toNumber(), 1000000000000000, 'balance is wrong');

    await contract.AddWhiteAddress(accounts[2]);
    await contract.approve(accounts[2], 2000, {from: accounts[1]});
    let allowance = await contract.allowance.call(accounts[1], accounts[2]);
    assert.equal(allowance.toNumber(), 2000, 'allowance is wrong');
    await contract.transferFrom(accounts[1], accounts[2], 2000, {from: accounts[2]});
    balance0 = await contract.balanceOf.call(accounts[0]);
    assert.equal(balance0.toNumber(), 1000000000000000, 'accounts[0] balance is wrong');
    balance1 = await contract.balanceOf.call(accounts[1]);
    assert.equal(balance1.toNumber(), 3000, 'accounts[1] balance is wrong');
    balance2 = await contract.balanceOf.call(accounts[2]);
    assert.equal(balance2.toNumber(), 2000, 'accounts[2] balance is wrong');
  });

  it("should reduce the total supply when tokens are given back (transfer)", async () => {
    await contract.issue(accounts[1], 5000);
    let totalSupply = await contract.totalSupply.call();
    assert.equal(totalSupply.toNumber(), 1000000000005000, 'total supply is wrong');
    let balance1 = await contract.balanceOf.call(accounts[1]);
    assert.equal(balance1.toNumber(), 5000, 'balance is wrong');
    let balance0 = await contract.balanceOf.call(accounts[0]);
    assert.equal(balance0.toNumber(), 1000000000000000, 'balance is wrong');  

    await contract.issue(accounts[1], 5000);
    totalSupply = await contract.totalSupply.call();
    assert.equal(totalSupply.toNumber(), 1000000000010000, 'total supply is wrong');
    balance1 = await contract.balanceOf.call(accounts[1]);
    assert.equal(balance1.toNumber(), 10000, 'balance is wrong'); 
    balance0 = await contract.balanceOf.call(accounts[0]);
    assert.equal(balance0.toNumber(), 1000000000000000, 'balance is wrong'); 

    await contract.AddWhiteAddress(accounts[0])
    await contract.transfer(accounts[0], 10000, {from: accounts[1]});
    balance1 = await contract.balanceOf.call(accounts[1]);
    assert.equal(balance1.toNumber(), 0, 'balance is wrong'); 
    balance0 = await contract.balanceOf.call(accounts[0]);
    assert.equal(balance0.toNumber(), 1000000000000000, 'balance is wrong');
    totalSupply = await contract.totalSupply.call();
    assert.equal(totalSupply.toNumber(), 1000000000000000, 'total supply is wrong');
  });


});