const MyIcoToken = artifacts.require("./MyIcoToken.sol");

const getAccounts = new Promise((resolve, reject) => {
  return web3.eth.getAccounts((err, acc) => {
    if (err) {
      reject(err);
    } else {
      resolve(acc);
    }
  })
});

async function setup(deployer) {
  let accounts = await getAccounts;
  await deployer.deploy(MyIcoToken);
}

module.exports = function(deployer) {
  setup(deployer);
};
